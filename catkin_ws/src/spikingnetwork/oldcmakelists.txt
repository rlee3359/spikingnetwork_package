cmake_minimum_required(VERSION 2.8.3)
set (CMAKE_CXX_STANDARD 11)

# Add sources - wildcard to collect all into SOURCE variable in a set
file(GLOB SOURCES "src/*.cpp")
include_directories(include)
# Include headers
# include_directories(include ${WIRINGPI_INCLUDE_DIRS}) 


add_executable(neuron ${SOURCES})

set(CMAKE_EXPORT_COMPILE_COMMANDS "ON")

# # Locate libraries and headers
# find_package(WiringPi REQUIRED)
# find_package(Threads REQUIRED) 
#
# # Link against libraries
# target_link_libraries(neuron ${WIRINGPI_LIBRARIES}) 
target_link_libraries(neuron ${CMAKE_THREAD_LIBS_INIT}) 


