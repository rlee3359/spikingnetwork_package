import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import math

voltages = pd.read_table('output.txt', ' ');
spikes = pd.read_table('spike_output.txt', ' ');

network_file = open("network.txt", "r")

neuron_nums = [int(n) for n in network_file.readline().strip('\n').split(',')]
print(neuron_nums)

lines = network_file.readlines()

cons = lines[1:]
cons = [c.strip("\n").split(",") for c in cons]
cons = [list(map(float, c)) for c in cons]
print(cons)

plt.figure(1)
i = 1
# voltages.drop(voltages.columns[len(voltages.columns)-1], axis=1, inplace=True)
L = len(voltages.columns)
# print(L)
# print(voltages.head())
for neuron in voltages:
    if 'NEURON' in neuron:
        plt.subplot(L, 1, i)
        voltages[neuron].plot(linewidth=1);
        s = spikes[neuron][spikes[neuron] == 1]
        plt.plot(s, 'r+', ms=1)

        if i <= neuron_nums[0]:
            title = "Input"
        elif i <= neuron_nums[0] + neuron_nums[1]:
            title = "Hidden Layer"
        elif i <= neuron_nums[0] + neuron_nums[1] + neuron_nums[2]:
            title = "Output Layer"

        plt.title(title, fontsize = 5)
        # spikes[neuron].plot();
        i += 1

offset = (math.ceil(neuron_nums[1]/3)) - 1
f2 = plt.figure(2)
ax = f2.add_subplot(111, aspect='equal')
iy = [y+offset for y in range(0,neuron_nums[0])]
ix = [5 for x in range(0,neuron_nums[0])]
# print(ix, iy)

hy = [y for y in range(0,neuron_nums[1])]
hx = [15 for x in range(0,neuron_nums[1])]

oy = [y+offset for y in range(0,neuron_nums[2])]
ox = [25 for x in range(0,neuron_nums[2])]



neuron_x = ix + hx + ox
neuron_y = iy + hy + oy

for c in cons:
    s = int(c[0])
    d = int(c[2])
    x = [c[1]*10+5, c[3]*10+5]
    # x = [neuron_x[s], neuron_x[d]]
    dy = neuron_y[d]
    if c[3] == 2:
        dy =neuron_y[d+1] 
    y = [neuron_y[s], dy]
    print(c, x, y)
    clr = (1, c[4], c[4])
    if c[5] > 0:
        clr = (c[4], c[4], 1)

    if x[0] == x[1] and y[0] == y[1]:
        p = patches.FancyArrowPatch((x[0], y[0]), (x[1]+0.01, y[1]+0.01), arrowstyle = '->', connectionstyle = 'arc3, rad=3', color = clr, mutation_scale=20)
        ax.add_patch(p)
    elif x[0] == x[1]:
        p = patches.FancyArrowPatch((x[0], y[0]), (x[1], y[1]), arrowstyle = '->', connectionstyle = 'arc3, rad=0.2', color = clr, mutation_scale=20)
        ax.add_patch(p)
    else:
        plt.plot(x, y, color = clr, linewidth=1) #str(c[2]))

plt.plot(ix, iy, 'go', ms = 10, markerfacecolor='w')
plt.plot(hx, hy, 'bo', ms = 10, markerfacecolor='w')
plt.plot(ox, oy, 'ro', ms = 10, markerfacecolor='w') 
plt.ylim([-1, neuron_nums[1]])
plt.show()

