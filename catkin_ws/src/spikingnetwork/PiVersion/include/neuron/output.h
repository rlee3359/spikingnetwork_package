#ifndef CONNECTION_H
#define CONNECTION_H

#include <wiringPi.h>

class Output {
    public:
        Output();
        void sendOutput(int spike);
};

#endif
