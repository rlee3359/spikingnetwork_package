#ifndef CONNECTION_H
#define CONNECTION_H

class Connection {
    public:
        int source;
        int destination;
        double weight;
        int type;

    Connection(int s, int d, double w, int t);
};

#endif
