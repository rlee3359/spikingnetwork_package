#ifndef NEURON_H 
#define NEURON_H 
#include <vector>
class Neuron {
    // Input parameters
    double a, b, c, d;
    std::vector<double> pwmWindow;

    // Membrane variables
    // v -> Membrane potential
    // u -> Membrane recovery variable
    double v, u;
    double input;

    double spikeThreshold;
    int spike;

    // Private Members
    void PrintStats();

    // Public Methods
    public:
    // Constructor - Takes parameters
    Neuron(double, double, double, double);
    void Print();
    void setInput(double i);

    // Step the neuron forward one timestep
    void Step();

    // Return the voltage 
    double getV();
    int getSpike();
};

#endif 
