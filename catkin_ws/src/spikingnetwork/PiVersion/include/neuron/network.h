#ifndef NETWORK_H
#define NETWORK_H

#include "neuron.h"
#include "connection.h"
#include <stdlib.h>

class Network {

    std::vector<Neuron> neurons;
    std::vector<Connection> connections;
    std::vector<int> spikeTrain;

    std::vector<std::vector<double> > neuronVoltages;
    std::vector<std::vector<double> > neuronSpikes;

    int numInputNeurons, numHiddenNeurons, numOutputNeurons;
    
    public:
        Network();
        void buildNetwork(int inputLayers, int hiddenLayers, int outputLayers);
        void connectNetwork();
        void step();

        void Store();
        void SaveData();
        int fixedInput;
};

#endif
