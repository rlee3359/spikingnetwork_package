#include <iostream>
#include <neuron/neuron.h>
#include <neuron/network.h>
#include <math.h>
#include <ctime>

int main(int argc, char **argv) {
    //Seed rand
    srand(time(NULL));
    // Initialize neuron
    Neuron neuron = Neuron(0.02, 0.2, -65.0, 8.0);

    // Create a network
    Network nn = Network();
    nn.buildNetwork(1, 3, 1);
    nn.connectNetwork();

    int cnt = 0;

    // Loop - generate signal
    while (1) {
        // Step the neuron simulation, input is 10Amps
        nn.step();
        // std_msgs::Float64 v = std_msgs::Float64();
        // v.data = nn.v;
        
        // Output voltage of one of the hidden neurons
        // voltagePub.publish(v);
        //
        
        cnt ++;

        nn.fixedInput = floor(cnt/100) * 5;
        std::cout << nn.fixedInput << std::endl;

        if (cnt > 500) {
            nn.SaveData();
            std::cout << "FILE STORED! \n \n \n \n";
            return 0;
        }
    }
    return 0;
}
