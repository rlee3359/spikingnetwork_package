#include <neuron/network.h>
#include <neuron/neuron.h>
#include <iostream>
#include <fstream>

#define RAND_TYPE (((rand() % 2) - 1) == 0) ? 50 : -50;
#define RAND_WEIGHT (rand() / (RAND_MAX + 1.))
// #define RAND_TYPE 1

Network::Network() {
    // buildNetwork(1, 1, 1);
}

// Contruct the network - Counts the total neurons and fills a vector of standard neurons
void Network::buildNetwork(int numInput, int numHidden, int numOutput) {
    // This implementation assumes all neurons have the same parameters (a,b,c,d)
    // Otherwise, revert back to three for loops with different params for i,h,o
    // Sum number of neurons to create all neurons
    numInputNeurons = numInput;
    numHiddenNeurons = numHidden;
    numOutputNeurons = numOutput;
    int totalNeurons = numInputNeurons + numHiddenNeurons + numOutputNeurons;

    // Create all neurons
    for (int i = 0; i < totalNeurons; i++) {
        // neurons.push_back(Neuron(0.02, 0.2, -65.0, 2.0));
        neurons.push_back(Neuron(0.1, 0.25, -55, 4));

        // Create containers for saving voltages to file
        neuronVoltages.push_back(std::vector<double>());
        neuronSpikes.push_back(std::vector<double>());
    }
}

// Create the network connections - Fills a vector of connections, connects all hidden neurons to eachother and inputs and outputs
// Recurrent connections are included, as well as connections between each hidden neuron
void Network::connectNetwork() {
    std::cout << "Connecting Network!" << std::endl;
    // Connect all hidden
    for (int h = 0; h < numHiddenNeurons; h++) {
        // Connect to input
        for (int i = 0; i < numInputNeurons; i++) {
            // Create connection values
            int src = i;
            int dst = numInputNeurons + h;
            double weight = RAND_WEIGHT;
            int type = RAND_TYPE;
            // Add connection to network
            connections.push_back(Connection(src, dst, weight, type));
        }
        // Connect to hidden
        for (int hh = 0; hh < numHiddenNeurons; hh++) {
            // Create connection values
            int src = numInputNeurons + hh;
            int dst = numInputNeurons + h;
            double weight = RAND_WEIGHT;
            int type = RAND_TYPE;
            // Add connection to network
            connections.push_back(Connection(src, dst, weight, type));
        }
        // Connect to output
        for (int o = 0; o < numOutputNeurons; o++) {
            // Create connection values
            int src = numInputNeurons + h;
            int dst = numInputNeurons + numHiddenNeurons + o;
            double weight = RAND_WEIGHT;
            int type = RAND_TYPE;
            // Add connection to network
            connections.push_back(Connection(src, dst, weight, type));
        }
    }

    // Print what was constructed
    std::cout << "Neuron count: " << neurons.size() << std::endl;
    std::cout << "Connection count: " << connections.size() << std::endl;
    std::cout << "Connections: \n"; 

    // Print all connection params - for debugging
    for (int c = 0; c < (int)connections.size(); c++) {
        std::cout << "  Connection " << c << "\n" << "Src: " << connections[c].source
            << "\n      Dst: " << connections[c].destination << "\nWeight: " << connections[c].weight
            << "\n      Type: " << connections[c].type << std::endl;
    }
 
}

// Step through the network
// Iterate through all neurons, find the connections that end with each, sum the inputs
// Then step through all with their inputs stored
void Network::step() {
    // Apply input to input neurons
    for (int i = 0; i < numInputNeurons; i++) {
        // Apply input to input neurons
        neurons[i].setInput(fixedInput); 
    }

    // Find the input for each neuron and step it
    for (int h = 0; h < numHiddenNeurons; h++) {
        double input = 0;
        int thisHidden = numInputNeurons + h;
        // Sum the outputs of all neurons through their connection to this one
        for (int c = 0; c < (int)connections.size(); c++) {
            // If this connection dest is this neuron
            if (connections[c].destination == thisHidden) {
                // Add the connection weight * neuron fire to input
                input += neurons[connections[c].source].getSpike() * connections[c].weight * connections[c].type;
            }
        }
        // Apply input to hidden neurons
        // std::cout << "Hidden Neuron " << thisHidden << " input: " << input << std::endl;
        neurons[thisHidden].setInput(input);
    }

    // Find input for each output neuron
    for (int o = 0; o < numOutputNeurons; o++) {
        double input = 0;
        // Find index for this neuron
        int thisOutput = numInputNeurons + numHiddenNeurons + o;
        // Sum the outputs of all neurons through their connection to this one
        for (int c = 0; c < (int)connections.size(); c++) {
            // If this connection dest is this neuron
            if (connections[c].destination == thisOutput) {
                // Add the connection weight * neuron fire to input
                input += neurons[connections[c].source].getSpike() * connections[c].weight * connections[c].type;
            }
        }
        // Apply input to output neurons 
        // std::cout << "Output Neuron " << thisOutput << " input: " << input << std::endl;
        neurons[thisOutput].setInput(input);
        // neurons[thisOutput].Print();
    }

    // Finally, step all neurons
    for (int n = 0; n < (int)neurons.size(); n++ ) {
        neurons[n].Step();

        // Print neuron variables
        // std::cout << " ~~NEURON " << n << "~~" << neurons[n].getSpike() << "|";
        // neurons[n].Print();
    }
    // std::cout << "\n";

    Store();
}

void Network::Store() {
    for (int n = 0; n < (int)neurons.size(); n++) {
        neuronVoltages[n].push_back(neurons[n].getV());
        neuronSpikes[n].push_back(neurons[n].getSpike());
    }

}

void Network::SaveData() {
    std::ofstream dataFile;
    std::ofstream spikeFile;
    std::ofstream networkFile;
    
    dataFile.open("output.txt");
    spikeFile.open("spike_output.txt");
    networkFile.open("network.txt");

    networkFile << numInputNeurons << "," << numHiddenNeurons << "," << numOutputNeurons << "\n";
    networkFile << "Connections s,d,w,t\n";
    
    for (int c = 0; c < (int)connections.size(); c++) {
        Connection con = connections[c];
        networkFile << con.source << "," << con.destination << "," << con.weight << "," << con.type << "\n"; 
    }

    for (int n = 0; n < (int)neurons.size(); n++) {
            std::string space = " ";
            // Sep with space if not end line
            if (n == (int)neurons.size() - 1) {
                space = "";
            }

         dataFile << "NEURON" << n << space;
         spikeFile << "NEURON" << n << space;
    }
    dataFile << std::endl;
    spikeFile << std::endl;
    
    // for (int n = 0; n < (int)neuronVoltages.size(); n++) {
    //     std::cout << neuronVoltages[n].size() << std::endl;
    // }

    for (int v = 0; v < (int)neuronVoltages[0].size(); v++) {
        for (int n = 0; n < (int)neurons.size(); n++) {
            // Seperate with a space, but only if not at the end of line
            std::string space = " ";
            if (n == (int)neurons.size() - 1) {
                space = "";
            }

            dataFile << neuronVoltages[n][v] << space; 
            spikeFile << neuronSpikes[n][v] << space;
        }
        dataFile << std::endl;
        spikeFile << std::endl;
    }

    //
    // for (int n = 0; n < (int)neurons.size(); n++) {
    //     dataFile << "NEURON:" << n << std::endl;
    //     for (int v = 0; v < (int)neuronVoltages[n].size(); v++) {
    //         dataFile << neuronVoltages[n][v] << std::endl;
    //     }
    // }
    dataFile.close();
    spikeFile.close();

}
