#include "wiringPi.h"
#include "neuron/output.h"
#include <iostream>
#define PIN 0

// Output Class Constructor
Output::Output() {
    // Set up GPIO library
    wiringPiSetup();
    // Set pin as output
    pinMode(PIN, OUTPUT);
}

void Output::sendOutput(int spike) {
    std::cout << "Sending output";
}
