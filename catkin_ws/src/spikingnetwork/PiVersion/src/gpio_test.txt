#include <wiringPi.h>
int main (void)
{
    wiringPiSetup();
    pinMode(0, OUTPUT);
    for(;;) {
        digitalWrite (0, HIGH);
        delay(5);
        digitalWrite (0,  LOW);
        delay(5);
    }
    return 0 ;
}
