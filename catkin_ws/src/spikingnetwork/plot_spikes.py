#!/usr/bin/env python


import matplotlib.pyplot as plt
import rospy
import roslib
from hector_uav_msgs.msg import MotorPWM

spike_train1 = []
spike_train2 = []
spike_train3 = []
spike_train4 = []

def callback(data):
    global spike_train
    spike_train1.append(data.pwm[0])
    plt.plot(spike_train1, c='b', linewidth=0.5)
    spike_train2.append(data.pwm[0])
    plt.plot(spike_train2, c='r', linewidth=0.5)
    spike_train3.append(data.pwm[0])
    plt.plot(spike_train3, c='y', linewidth=0.5)
    spike_train4.append(data.pwm[0])
    plt.plot(spike_train4, c='k', linewidth=0.5)



    plt.pause(0.0001)



plt.ion()

def listener():
    global spike_train
    rospy.init_node('spike_plot', anonymous=True)
    rospy.Subscriber("/motor_pwm", MotorPWM, callback)
    rospy.spin()

listener()

