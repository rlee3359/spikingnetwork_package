#!/bin/bash

echo "Running Neural Network"
./build/neuron
echo "Plotting Network Output"
python3 ./plot_network.py
