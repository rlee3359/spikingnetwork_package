#ifndef CONNECTION_H
#define CONNECTION_H

#include "spikingnetwork/connection.h"
#include "spikingnetwork/neuron.h"
#include <list>
#include <iterator>

class Connection {
    public:
        std::list<Neuron>::iterator source;
        std::list<Neuron>::iterator destination;

        double weight;
        int type;

    Connection();
    Connection(std::list<Neuron>::iterator s, std::list<Neuron>::iterator d, double w, int t);
    // Connection(const Connection &c);
};

#endif
