#include <iostream>
#include <spikingnetwork/neuron.h>
#include <spikingnetwork/network.h>
#include <math.h>
#include <ctime>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

// ROS
#include "ros/ros.h"
// #include <hector_uav_msgs/MotorPWM.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Range.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Twist.h>

#define RAND_WEIGHT (rand() / (RAND_MAX + 1.))

void saveFitnesses(std::vector<double> fitnesses);
void hillClimber(int argc, char **argv);
void parralelHillClimber(int argc, char **argv);
void test();
void pulseWidthTest();
void testPWM(int argc, char **argv);
double evaluateFitness(Network &network);


int main(int argc, char **argv) {
    //Seed rand
    srand(time(NULL));




    //
    // if (RAND_WEIGHT < sensorNormalized) {
    //     spike = 1;
    // } else {
    //     spike = 0;
    // }
    // Network nn = Network();
    // nn.buildNetwork(3,5,3);
    // nn.addNeuron();
    // nn.addNeuron();
    // nn.addNeuron();
    // nn.addNeuron();
    // nn.SaveData(0);
    //
    //
    // Network nf = Network();
    // nf.buildNetworkFromFile(0);
    // nf.SaveData(100);

    // hillClimber(argc, argv);
    parralelHillClimber(argc, argv);
    // test();
    // pulseWidthTest();
    //
    // testPWM(argc, argv);

    return 0;
}

void saveFitnesses(std::vector<double> fitnesses) {
    std::cout << "SAVING" << std::endl;
    std::ofstream fitnessFile;
    std::string fitnesssFileName = "/home/robert/catkin_ws/src/spikingnetwork/network_output/fitnesses.txt";
    fitnessFile.open(fitnesssFileName, std::ios_base::app);

    fitnessFile << std::endl;
    for (auto f = fitnesses.begin(); f != fitnesses.end(); ++f) {
        fitnessFile << *f << ",";
    }

    fitnessFile.close();

    return;
}


ros::NodeHandle * pn;
double evaluateFitness(Network &network) {
    double averageSize = 1.0;
    double sumFitness = 0.0;
    for (int i = 0; i < averageSize; i++) {
        ros::Rate rate(100.0);
        ros::start();
        // ros::Publisher pwmPublisher = pn->advertise<hector_uav_msgs::MotorPWM>("/motor_pwm", 100);
        ros::Publisher velPublisher = pn->advertise<geometry_msgs::Twist>("/cmd_vel", 100);

        ros::ServiceClient resetClient = pn->serviceClient<std_srvs::Empty>("/gazebo/reset_world");

        std_srvs::Empty resetCall;
        // network.seconds = 0;


        ros::Subscriber stateSubscriber = pn->subscribe("/ground_truth/state", 1000, &Network::stateInputCallback, &network);
        // ros::Subscriber timeSubscriber = n.subscribe("/clock", 1000, &Network::timeCallback, &network);

        network.reset();

        resetClient.call(resetCall);
        double duration = 0;
        double cummulativeError = 0;

        // Time::now() returns 0 until the first actual time is recieved on /clock
        while(ros::Time::now().toSec() == 0) {
            // Wait till time is recieved
        }


        double timeStepWeight = 1.0;
        double startTime = ros::Time::now().toSec();

        // Set quad as upright
        network.crash = false;
        // std::cout << "Crash: " << network.crash << std::endl;

        while (ros::ok() && duration < 10 && network.crash == false) {
            // std::cout << "timenow " << ros::Time::now().toSec() << std::endl;
            duration = ros::Time::now().toSec() - startTime;
        
            std::vector<double> pwmVector = network.step();

            // std::vector<unsigned char> charVec;

            geometry_msgs::Twist velocityOutput;
            // for (auto i = pwmVector.begin(); i != pwmVector.end(); ++i) {
            //     velocityOutput.linear.z = *i;
            //     // charVec.push_back((*i) * 255);
            // }
            
            // Only one output at the moment
            velocityOutput.linear.z = pwmVector[0] - 0.4;

            // hector_uav_msgs::MotorPWM pwmMsg;
            // pwmMsg.pwm = charVec;
            // pwmMsg.header.stamp = ros::Time::now();
            
            // velocityOutput.header.stamp = ros::Time::now();

            // Publish the output
            // pwmPublisher.publish(pwmMsg);
            velPublisher.publish(velocityOutput);


            // Fitness is a combination of error over time and time steps alive, promoting nets
            // that last longer without crashing
            // heigh minus ground height
            // cummulativeError += network.height - 0.182462435066;
            cummulativeError -= network.error;
            // std::cout << "TIME: " << duration << std::endl;

            ros::spinOnce();
            rate.sleep();
        }
        // std::cout << "Test num: " << i << ", " << cummulativeError << std::endl;

        sumFitness += cummulativeError;
    }

    double averagedFitness = sumFitness/averageSize;
    // std::cout << "Fitness for this run: " << averagedFitness << std::endl;
        

    // // If the quad goes down, fitness is awful
    // if (network.crash == true) {
    //     // cummulativeError += 10000;
    //     cummulativeError = 1;
    // }

    return averagedFitness;
}


//fill popuation up to 20
//evaluate all 20
//
////generation start
//in each gen
//for (net in population)
//  create 4 children, mutate,  and get their fitness
//  we have 5 possible parents, pick the fittest )(may be previous parent)
//  add the best to that population slot


void parralelHillClimber(int argc, char **argv) {
    std::string fitnesssFileName = "/home/robert/catkin_ws/src/spikingnetwork/network_output/fitnesses.txt";
    std::ofstream fitnessFile;
    fitnessFile.open(fitnesssFileName);
    fitnessFile.close();

    ros::init(argc, argv, "SpikingNetwork");
    ros::NodeHandle n;
    pn = &n;

    int numOfGens = 5000;
    int numChildren = 4;
    int populationSize = 20;

    // Population and corressponding fitnessess
    std::vector<Network> population;
    std::vector<double> fitnessess;
    for (int p = 0; p != 20; p++) {
        population.push_back(Network());
        population[p].buildNetwork(2,4,1);
        population[p].connectNetwork();

        // Evaluate fitnesses initially
        fitnessess.push_back(evaluateFitness(population[p]));
    }
    //
    //
    // Network nn = Network();
    // nn.buildNetworkFromFile(3);
    // nn.print();
    // while(1) {
    //     std::cout << evaluateFitness(nn) << std::endl;
    //     nn.SaveData(0);
    // }
    
    // for (int p = 0; p < 20; p++) {
    //     population.push_back(Network());
    //     population[p].buildNetworkFromFile(p);
    //     // Evaluate fitnesses initially
    //     fitnessess.push_back(evaluateFitness(population[p]));
    // }

    saveFitnesses(fitnessess);

    // All generations
    for (int currentGen = 0; currentGen < numOfGens; currentGen++) {
        std::cout << "GENERATION: " << currentGen << std::endl;
        // For all parent networks
        // for (auto parent = population.begin(); parent != population.end(); ++parent) {
        for (int p = 0; p != 20; p++) {
            std::cout << "Parent Number: " << p << std::endl;
            // population[p].reset();
            // Create new children vector and fitness vector 
            std::vector<Network> children;
            // std::vector<double> fitnesses;

            // std::cout << "Parent fitness: " << parentFitness << "\n   Children: "<< std::endl;
            double bestChildFitness = -100000000;
            int bestChild;
            // Create a new set of children, mutate 
            for (int ch = 0; ch != numChildren; ch++) {
                std::cout << "Child Num: " << ch << std::endl;
                children.push_back(Network());
                children[ch].buildNetworkFromParent(population[p]);
                children[ch].mutate();
                // children[ch].reset();

                // Get the best child
                double childFitness = evaluateFitness(children[ch]);
                // std::cout << childFitness << std::endl;
                if (childFitness > bestChildFitness) {
                    bestChild = ch;
                    bestChildFitness = childFitness;
                }
            }

            // Check if better than parent
            if (bestChildFitness > fitnessess[p]) {
                children[bestChild].SaveData(p);
                population[p] = Network();
                population[p].buildNetworkFromParent(children[bestChild]);
                fitnessess[p] = bestChildFitness;
            } else {
                population[p].SaveData(p);
            }

        }

        // SAVE THE CURRENT FITTEST PARENTS TO THE FILE
        // std::vector<double> finalFitnesses;
        saveFitnesses(fitnessess);

        // // int fittestIndex;
        // // double bestFitness = 100000;
        // for (int p = 0; p != populationSize; p++) {
        //     // population[p].reset();
        //     // double thisFitness = population[p].getFitness();
        //     // if (thisFitness < bestFitness) {
        //     //     bestFitness = thisFitness;
        //     //     fittestIndex = p;
        //     // }
        //     // finalFitnesses.push_back(thisFitness);
        //     population[p].SaveData(p);
        // }

        // sleep(5);
        // population[fittestIndex].print();
        // DONE
    }
    // Left with a vector of the fittest parents
    // std::vector<double> finalFitnesses;
    //
    // int fittestIndex;
    // double bestFitness = 100000;
    // for (int p = 0; p != populationSize; p++) {
    //     population[p].reset();
    //     double thisFitness = population[p].getFitness();
    //     if (thisFitness < bestFitness) {
    //         bestFitness = thisFitness;
    //         fittestIndex = p;
    //     }
    //     finalFitnesses.push_back(thisFitness);
    // }
    //
    // std::cout << "Final fitness: " << bestFitness << std::endl;
    //
    // population[fittestIndex].print();
    // population[fittestIndex].SaveData(20);
}

// double runOneExperimentGetFitness(Network &nn) {
//     double fitness = 0;
//
//     ros::Subscriber stateSubscriber = n.subscribe("/ground_truth/state", 1000, &Network::stateInputCallback, &parent);
//     ros::Subscriber timeSubscriber = n.subscribe("/clock", 1000, &Network::timeCallback, &parent);
//
//     
//     // Loop - generate signal
//     while (ros::ok() and nn.seconds < 30) {
//         nn.fixedInput = 10;
//         std::vector<int> pwmVector = nn.step();
//
//         std::vector<unsigned char> charVec;
//
//         for (auto i = pwmVector.begin(); i != pwmVector.end(); ++i) {
//             charVec.push_back((*i) * 255);
//         }
//
//         hector_uav_msgs::MotorPWM pwmMsg;
//         pwmMsg.pwm = charVec;
//         pwmMsg.header.stamp = ros::Time::now();
//
//         // Publish the output
//         pwmPublisher.publish(pwmMsg);
//
//
//         // cnt ++;
//         //
//         // if (cnt > 300) {
//         //     // nn.SaveData(0);
//         //     // std::cout << "FILE STORED! \n \n \n \n";
//         //     return;
//         //
//         // }
//         //
//         
//         ros::spinOnce();
//         rate.sleep();
//     }
//
//     fitness = nn.error^2;
//     return fitness;
// }



void hillClimber(int argc, char **argv) {
    ros::init(argc, argv, "SpikingNetwork");
    ros::NodeHandle n;
    pn = &n;

    // Create a network
    Network parent = Network();
    parent.buildNetwork(2, 15, 1);
    parent.connectNetwork();

    // for (int i = 0; i < 100; i++) {
    //     std::cout << evaluateFitness(parent) << std::endl;
    // }

    std::ofstream fitnessFile;
    std::string fitnesssFileName = "/home/robert/catkin_ws/src/spikingnetwork/network_output/fitness_hill.txt";
    fitnessFile.open(fitnesssFileName);
    fitnessFile.close();

    double parentFitness = 0;
    parentFitness = evaluateFitness(parent);

    int numOfGens = 2000;
    for (int currentGen = 0; currentGen < numOfGens; currentGen++) {
        // Create new child from parent
        Network child = Network();
        child.buildNetworkFromParent(parent);

        // Mutate Child
        child.mutate();

        // Get the fitness of each - average over 5 runs because of noise
        double childFitness = 0;
        childFitness = evaluateFitness(child);

        // Replace parent if child is better
        if (childFitness > parentFitness) {
            // std::cout << "Parent replaced\n";
            parentFitness = childFitness;
            std::cout << parentFitness << std::endl;
            parent = Network();
            parent.buildNetworkFromParent(child);
            saveFitnesses(std::vector<double> (1, childFitness));
        } else {
            saveFitnesses(std::vector<double> (1, parentFitness));
        }

        fitnessFile.open(fitnesssFileName, std::ios_base::app);
        fitnessFile << parentFitness << ",";
        fitnessFile.close();
        // std::cout << "Saving Data" << std::endl;
        parent.SaveData(0);
    }

}

void pulseWidthTest() {
    std::vector<int> sizes = {1, 5, 10, 50, 100, 250, 500, 750, 1000};

    // Create networks of different sizes and step through them
    for (auto s = sizes.begin(); s != sizes.end(); ++s) {
        // Create the new network!
        Network testNet = Network();
        testNet.buildNetwork(1, *s, 1);
        testNet.connectNetwork();
        testNet.fixedInput = 10;

        std::cout << "Testing network with " << *s << " hidden neurons." << std::endl;
        // Step through for 1000 timesteps
        for (int ts = 0; ts < 1000; ts++) {
            testNet.step();
        }
        sleep(2);

    }
}


void test() {
    // Initialize neuron
    // Neuron neuron = Neuron(0.02, 0.2, -65.0, 8.0);

    // Create a network
    Network nn = Network();
    nn.buildNetwork(2, 3, 1);
    nn.connectNetwork();
    // nn.removeConnection();
    // nn.mutateConnectionWeights(0.5);
    // nn.addConnection();
    // nn.addNeuron();
    // nn.removeNeuron();

    int cnt = 0;

    // Loop - generate signal
    while (1) {
        // child.step();
        // std_msgs::Float64 v = std_msgs::Float64();
        // v.data = nn.v;

        // Output voltage of one of the hidden neurons
        // voltagePub.publish(v);
        //
        nn.step();

        cnt ++;

        // child.fixedInput = floor(cnt/100) * 5;
        // std::cout << nn.fixedInput << std::endl;

        if (cnt > 300) {
            nn.SaveData(0);
            //
            // std::cout << "FILE STORED! \n \n \n \n";
            return;
        }
    }
}

void testPWM(int argc, char **argv) {
    //
    // ros::init(argc, argv, "SpikingNetwork");
    // ros::NodeHandle n;
    // ros::Rate rate(100.0);
    // ros::Publisher pwmPublisher = n.advertise<hector_uav_msgs::MotorPWM>("/motor_pwm", 100);
    //
    // ros::ServiceClient resetClient = n.serviceClient<std_srvs::Empty>("/gazebo/reset_world");
    //
    // std_srvs::Empty resetCall;
    // resetClient.call(resetCall);
    //
    // // Initialize neuron
    // // Neuron neuron = Neuron(0.02, 0.2, -65.0, 8.0);
    //
    // // Create a network
    // Network nn = Network();
    // nn.buildNetwork(2, 3, 4);
    // nn.connectNetwork();
    //
    // ros::Subscriber stateSubscriber = n.subscribe("/ground_truth/state", 1000, &Network::stateInputCallback, &nn);
    // // ros::Subscriber timeSubscriber = n.subscribe("/clock", 1000, &Network::timeCallback, &nn);
    //
    // int cnt = 0;
    //
    //
    // // Loop - generate signal
    // while (ros::ok()) {
    //     nn.fixedInput = 10;
    //     std::vector<double> pwmVector = nn.step();
    //
    //     std::vector<unsigned char> charVec;
    //
    //     for (auto i = pwmVector.begin(); i != pwmVector.end(); ++i) {
    //         charVec.push_back((*i) * 255);
    //     }
    //
    //     hector_uav_msgs::MotorPWM pwmMsg;
    //     pwmMsg.pwm = charVec;
    //     pwmMsg.header.stamp = ros::Time::now();
    //
    //     // Publish the output
    //     pwmPublisher.publish(pwmMsg);
    //
    //
    //     // cnt ++;
    //     //
    //     // if (cnt > 300) {
    //     //     // nn.SaveData(0);
    //     //     // std::cout << "FILE STORED! \n \n \n \n";
    //     //     return;
    //     //
    //     // }
    //     //
    //     
    //     ros::spinOnce();
    //     rate.sleep();
    // }
}

