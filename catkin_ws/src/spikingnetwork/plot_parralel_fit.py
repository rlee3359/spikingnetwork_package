import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


fitness = pd.read_csv('./network_output/fitnesses.txt', header=None)

fitnesses = []
for i in range(0,20):
    fitnesses.append(fitness.iloc[:,i])
    
for fit in fitnesses:
    fit.plot()
    
plt.title('Parent Fitness')
plt.xlabel('Generation')
plt.ylabel('Fitness')
plt.show()





